const fs = require("fs")
const gulp = require("gulp")
const sass = require("gulp-sass")
const concat = require("gulp-concat")
const cssmin = require("gulp-cssmin")
const rename = require("gulp-rename")
const postcss = require("gulp-postcss")
const webserver = require("gulp-webserver")
const autoprefixer = require("autoprefixer")
const babel = require("gulp-babel")
const portfinder = require("portfinder")

const source = {
		html: "",
		javascript: ["src/js/**/*.js", "!src/js/min/**/*.js"],
		scss: "src/sass/**/*.scss"
	},
	destination = {
		javascript: "dist/assets/js",
		scss: "dist/assets/css"
	}

gulp.task("sass", () => {
	gulp
		.src(source.scss)
		.pipe(sass().on("error", sass.logError))
		.pipe(postcss([autoprefixer({ browsers: ["last 38 versions"] })]))
		.pipe(gulp.dest(destination.scss))
		.pipe(cssmin())
		.pipe(
			rename(path => {
				path.extname = ".min.css"
			})
		)
		.pipe(gulp.dest(destination.scss))
})

gulp.task("webserver", () => {
	portfinder.getPort((err, port) => {
		gulp.src("./" + source.html).pipe(
			webserver({
				host: "0.0.0.0",
				livereload: true,
				directoryListing: {
					enable: false,
					path: source.html
				},
				open: true,
				port: port,
				fallback: "index.html"
			})
		)
	})
})

gulp.task("javascript", () => {
	gulp
		.src(source.javascript)
		.pipe(
			babel({
				presets: [
					[
						"env",
						{
							useBuiltIns: true
						}
					]
				],
				plugins: [
					["transform-object-rest-spread", { useBuiltIns: true }],
					"syntax-object-rest-spread",
					"transform-regenerator",
					"babel-plugin-syntax-async-functions"
				]
			})
		)
		.pipe(concat("bundle.js"))
		.pipe(gulp.dest(destination.javascript))
})

gulp.task("build", ["sass", "javascript"])

gulp.task("watch", ["webserver"], () => {
	gulp.watch(source.scss, ["sass"])
	gulp.watch(source.javascript, ["javascript"])
})
