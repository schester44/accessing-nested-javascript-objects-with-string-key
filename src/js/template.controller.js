const flatten = (obj, prefix = "") => {
	return Object.keys(obj).reduce((acc, key) => {
		const curr = obj[key]

		const currPrefix = `${prefix}${prefix !== "" ? "." : ""}${key}`

		if (typeof curr === "string") {
			return {
				...acc,
				[currPrefix]: curr
			}
		}

		if (curr && curr.constructor.name.toLowerCase() === "object") {
			return {
				...acc,
				...flatten(curr, currPrefix)
			}
		}

		return acc
	}, {})
}

const mapFields = (fields, events, validator) => {
	const mappingKeys = Object.keys(fields)
	const res = []

	for (let i = 0; i < events.length; i++) {
		const event = flatten(events[i])

		if (!!validator && !validator(event)) continue

		res.push(
			mappingKeys.reduce(
				(acc, key) => ({
					...acc,
					[key]: event[fields[key]]
				}),
				{}
			)
		)
	}

	return res
}

function TemplateController(iwDataService, $interval) {
	let device_id = 123

	this.pagination = {
		perPage: 1,
		currentPage: 1,
		slideDuration: 1
	}

	iwDataService.get({ service: "live", url: "appc" }).then(server => {
		if (!server.data) {
			console.log(server)
			throw new Error("Request error")
		}

		const device = server.data.devices.find(({ id }) => +id === +device_id)
		this.events = []

		if (device) {
			this.events = mapFields(server.settings.fieldMappings, server.data.events, event => {
				return event["room.id"] === device.room_id
			})
		}

		const totalPages = Math.max(Math.ceil(this.events.length / this.pagination.perPage), 1)
		this.pagination.totalPages = totalPages <= 20 ? totalPages : 20

		if (this.pagination.totalPages > 1) {
			if (this.pagination.slideDuration > 0) {
				$interval(() => {
					this.pagination.currentPage++

					if (this.pagination.currentPage > this.pagination.totalPages) {
						this.pagination.currentPage = 1
					}
				}, this.pagination.slideDuration * 3000)
			}
		}
	})
}

const app = angular.module("app")
app.controller("TemplateController", ["iwDataService", "$interval", TemplateController])
