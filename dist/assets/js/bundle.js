"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var flatten = function flatten(obj) {
	var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";

	return Object.keys(obj).reduce(function (acc, key) {
		var curr = obj[key];

		var currPrefix = "" + prefix + (prefix !== "" ? "." : "") + key;

		if (typeof curr === "string") {
			return Object.assign({}, acc, _defineProperty({}, currPrefix, curr));
		}

		if (curr && curr.constructor.name.toLowerCase() === "object") {
			return Object.assign({}, acc, flatten(curr, currPrefix));
		}

		return acc;
	}, {});
};

var mapFields = function mapFields(fields, events, validator) {
	var mappingKeys = Object.keys(fields);
	var res = [];

	var _loop = function _loop(i) {
		var event = flatten(events[i]);

		if (!!validator && !validator(event)) return "continue";

		res.push(mappingKeys.reduce(function (acc, key) {
			return Object.assign({}, acc, _defineProperty({}, key, event[fields[key]]));
		}, {}));
	};

	for (var i = 0; i < events.length; i++) {
		var _ret = _loop(i);

		if (_ret === "continue") continue;
	}

	return res;
};

function TemplateController(iwDataService, $interval) {
	var _this = this;

	var device_id = 123;

	this.pagination = {
		perPage: 1,
		currentPage: 1,
		slideDuration: 1
	};

	iwDataService.get({ service: "live", url: "appc" }).then(function (server) {
		if (!server.data) {
			console.log(server);
			throw new Error("Request error");
		}

		var device = server.data.devices.find(function (_ref) {
			var id = _ref.id;
			return +id === +device_id;
		});
		_this.events = [];

		if (device) {
			_this.events = mapFields(server.settings.fieldMappings, server.data.events, function (event) {
				return event["room.id"] === device.room_id;
			});
		}

		var totalPages = Math.max(Math.ceil(_this.events.length / _this.pagination.perPage), 1);
		_this.pagination.totalPages = totalPages <= 20 ? totalPages : 20;

		if (_this.pagination.totalPages > 1) {
			if (_this.pagination.slideDuration > 0) {
				$interval(function () {
					_this.pagination.currentPage++;

					if (_this.pagination.currentPage > _this.pagination.totalPages) {
						_this.pagination.currentPage = 1;
					}
				}, _this.pagination.slideDuration * 3000);
			}
		}
	});
}

var app = angular.module("app");
app.controller("TemplateController", ["iwDataService", "$interval", TemplateController]);